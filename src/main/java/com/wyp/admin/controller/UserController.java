package com.wyp.admin.controller;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.LambdaUpdateWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.wyp.admin.common.Result;
import com.wyp.admin.common.Token;
import com.wyp.admin.entity.User;
import com.wyp.admin.mapper.UserMapper;
import com.wyp.admin.vo.UserVo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.lang.reflect.Array;
import java.util.List;


/**
 * @author Larry
 */
@CrossOrigin
@RestController
@RequestMapping(value = "/user", produces = "application/json;charset=UTF-8")
@Api(value = "用户接口", tags = "用户管理相关的接口")
public class UserController {
    @Autowired
    private UserMapper userMapper;

    /**
     * 用户注册功能
     * @param username
     * @param phonenumber
     * @param password
     * @return
     */
    @PostMapping(value = "/regist", produces ={"application/json;charset=UTF-8"})
    @ResponseBody
    @ApiOperation(value = "用户注册", notes = "新增用户")
    public Result regist(@RequestParam("username") String username, @RequestParam("phonenumber") String phonenumber, @RequestParam("password") String password) {
        User user = new User();
        user.setUsername(username);
        user.setPassword(password);
        user.setPhonenumber(phonenumber);
        QueryWrapper<User> wrapper = new QueryWrapper<User>();
        wrapper.eq("username",user.getUsername());
        try {
            Long count = userMapper.selectCount(wrapper);
            if (count == 0) {
                int result = userMapper.insert(user);
                if (result == 1) {

                    System.out.println("用户注册成功");
                    return Result.success();
                } else {
                    return Result.fail(301, "系统业务繁忙，请联系管理员");
                }
            } else {
                return Result.fail(311,"用户名重复！");
            }
        } catch (Exception e) {
            System.out.println("报错： " + e);
            return Result.fail(301, "msg");
        }
    }

    /**
     * 用户登录功能
     * @param username
     * @param password
     * @return Result
     */
    @PostMapping("/login")
    @ApiOperation(value = "用户登录", notes = "用户登录")
    public Result login(@RequestParam("username") String username, @RequestParam("password") String password) {
        User user = new User();
        user.setUsername(username.trim());
        if (user.getUsername().contains(" ")) {
            return Result.fail(311, "用户名不能含有” “（空格）");
        }
        user.setPassword(password);
        QueryWrapper<User> wrapper = new QueryWrapper<User>();
        wrapper.eq("username",user.getUsername());
        List<User> list = userMapper.selectList(wrapper);
        if (list.get(0).getPassword().equals(user.getPassword())) {
            System.out.println("核实密码正确");
            String token = Token.generateToken("secret", 4, user.getUsername());
            UserVo userVo = new UserVo();
            userVo.setUsername(list.get(0).getUsername());
            userVo.setPhonenumber(list.get(0).getPhonenumber());
            return Result.loginSsuccess(token, userVo);
        } else {
            return Result.fail(311, "密码错误");
        }
    }
    /**
     * 分页查询所有数据
     *
     */
    @PostMapping("/selectByPage")
    @ApiOperation(value = "查询所有用户", notes = "查询所有用户")
    private Result selectByPage(@RequestParam(defaultValue = "1") Number pageNum, @RequestParam(defaultValue = "20") Number pageSize) {
        try {
            Page<User> page = new Page<>(pageNum.intValue(), pageSize.intValue());
            return Result.success(userMapper.selectUserPage(page));
        }catch (Exception e) {
            return Result.fail(301, e.toString());
        }
    }
    @PostMapping("/deleteById")
    @ApiOperation(value = "删除用户", notes = "删除用户")
    private Result deleteById(@RequestParam int id) {
        try {
            userMapper.deleteById(id);
            return Result.success();
        }catch (Exception e) {
            return Result.fail(301,"删除失败"+e);
        }
    }

    @PostMapping("/alterUser")
    @ApiOperation(value = "修改用户信息", notes = "修改用户信息")
    private Result alterUser(@RequestParam int id, @RequestParam String phoneNumber, @RequestParam String password){
        LambdaUpdateWrapper<User> lambdaUpdateWrapper = new LambdaUpdateWrapper<>();
        lambdaUpdateWrapper.eq(User::getId, id).set(User::getPhonenumber, phoneNumber).set(User::getPassword, password);
        Integer rows = userMapper.update(null, lambdaUpdateWrapper);
        System.out.println(rows);
        return Result.success();
    }
}
