package com.wyp.admin.entity;


import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data
@TableName("user")
public class User {
    @TableId(type = IdType.AUTO)//解决insert注入数据时报 value of "id" is
    private Long id;

    @TableField("username")
    private String username;

    @TableField("phonenumber")
    private String phonenumber;

    @TableField("password")
    private String password;
}
