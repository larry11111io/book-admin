package com.wyp.admin.vo;

import lombok.AllArgsConstructor;
import lombok.Data;

@AllArgsConstructor
@Data
public class UserVo {
    private String username;
    private String phonenumber;

    public UserVo() {

    }
}
