package com.wyp.admin.common;

import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class RequestInterceptor extends HandlerInterceptorAdapter {

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        try {
            String token = request.getHeader("Authorization");
            System.out.println("token = " + token);
            if(token.isEmpty() || token == null) {
                System.out.println("token为空，验证失败");
                response.sendError(401);
                return false;
            }else if (Token.getClaimByToken(token) == null) {
                System.out.println("token验证异常，验证失败");
                response.sendError(401);
                return false;
            }
            System.out.println("验证成功，返回true");
            return true;
        } catch (Exception e) {
            response.sendError(401);
            return false;
        }
    }
}
