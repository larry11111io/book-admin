package com.wyp.admin.common;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;

import java.util.Date;

public class Token {

    private String secretKey = "secret";

    /**
     * 生成token
     * @return token
     * @Param  对象map结构
     */

    public static String generateToken(String secret, int expire, String username){
        Date date = new Date();
        Date expireTime = new Date(date.getTime() + expire * 60 * 60 * 1000);
        String token = Jwts.builder()
                .setHeaderParam("typ", "JWT")//数据
                .claim("username", username)
                .setIssuedAt(date)
                .setExpiration(expireTime)	//过期时间
                .signWith(SignatureAlgorithm.HS256, secret) //秘钥
                .compact();

        return token;
    }

    /**
     * 验证token
     * @return  token正确返回对象，token不正确返回null
     */
    public static String getClaimByToken(String token){
        try {
            Claims claims = Jwts.parser()
                    .setSigningKey("secret")  //获取秘钥
                    .parseClaimsJws(token)	//解析验证token
                    .getBody();
            return claims.get("username", String.class);
        }catch (Exception e){
            System.out.println("验证失败,报错内容：{ " + e + " }");
            return null;
        }
    }

}
