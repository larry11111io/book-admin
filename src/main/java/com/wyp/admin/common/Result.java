package com.wyp.admin.common;
import java.io.Serializable;

import com.wyp.admin.entity.User;
import com.wyp.admin.vo.UserVo;
import lombok.*;

@Data
public class Result<T> implements Serializable {
    private static final long serialVersionUID = 1L;
    private int code;
    private String msg;
    private T data;
    private String token;

    public static <T> Result<T> success(T data) {
        return new Result<T>(200, "success", data, null);
    }

    public static <T> Result<T> success() {
        return new Result<T>(200, "success", null, null);
    }
    public static <T> Result<T> loginSsuccess(String token, UserVo userVo) {
        return new Result<T>(200, "success", (T) userVo, token);
    }

    public static <T> Result<T> fail(int code, String msg) {
        return new Result<T>(code, msg, null, null);
    }

    public Result() {
    }

    public Result(int code, String msg, T data, String token) {
        this.code = code;
        this.msg = msg;
        this.data = data;
        this.token = token;
    }


}

